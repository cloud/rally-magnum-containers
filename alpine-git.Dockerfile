FROM alpine:3.15

RUN apk add --no-cache ca-certificates curl jq bash openssl git
RUN apk add --no-cache yq --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community
