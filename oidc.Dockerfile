FROM registry.cern.ch/cloud/ciadm:v0.3.11

COPY oidc-magnum-cli.patch .
RUN git apply --unsafe-paths --directory=/usr/lib/python2.7/site-packages/ oidc-magnum-cli.patch

# python3
RUN yum -y install python2 python2-pip python3 python3-pip \
	&& yum clean all

RUN pip3 install --no-cache-dir pexpect

RUN curl -LO https://github.com/int128/kubelogin/releases/download/v1.22.1/kubelogin_linux_amd64.zip \
    && unzip kubelogin_linux_amd64.zip -d /usr/bin/
