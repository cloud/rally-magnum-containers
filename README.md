> :warning: This repository [has moved](https://gitlab.cern.ch/kubernetes/storage/eosxd). The new location is a fork and not a repository migration so that old container registry images were kept.


# rally-magnum-containers

Container images for Magnum tests in Rally.

Prefer slim images where possible.
