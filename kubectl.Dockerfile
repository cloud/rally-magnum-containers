FROM alpine:3.15

ARG KUBE_VERSION=v1.22.2

RUN apk add --no-cache ca-certificates curl jq bash openssl

RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/${KUBE_VERSION}/bin/linux/amd64/kubectl && \
    mv kubectl /usr/bin/kubectl && \
    chmod +x /usr/bin/kubectl
